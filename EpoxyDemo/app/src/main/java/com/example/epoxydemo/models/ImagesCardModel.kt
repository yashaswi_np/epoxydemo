package com.example.epoxydemo.models

/**
 * Created by Yashaswi NP on 28/10/19.
 */
data class ImagesCardModel (
    val firstImageLogo : Int = -1,
    val secondImageLogo : Int = -1,
    val firstImageTitle : String  = "",
    val secondImageTitle : String = ""
)

