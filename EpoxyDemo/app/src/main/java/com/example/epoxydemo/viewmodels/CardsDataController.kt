package com.example.epoxydemo.viewmodels

import android.content.Context
import com.airbnb.epoxy.EpoxyController
import com.example.epoxydemo.models.CarouselCardModel
import com.example.epoxydemo.models.ImageDescriptionCardModel
import com.example.epoxydemo.models.ImagesCardModel
import com.example.epoxydemo.models.SingleDescriptionCardModel
import com.example.epoxydemo.repositories.CardsDataRepository
import com.example.epoxydemo.views.adapters.*
import kotlin.coroutines.coroutineContext


/**
 * Created by Yashaswi NP on 28/10/19.
 */
class CardsDataController(var context: Context) : EpoxyController(){

    var singleDescCrad : List<SingleDescriptionCardModel>
    var imageDescCard : List<ImageDescriptionCardModel>
    var imagesCard : List<ImagesCardModel>
    var carouselCard : List<CarouselCardModel>

    init {
        //TODO this logic changes if the data comes from remote
        carouselCard = CardsDataRepository.getCarouselCardData(1)
        singleDescCrad = CardsDataRepository.getSingleDescData(5)
        imageDescCard = CardsDataRepository.getImageDescData(5)
        imagesCard = CardsDataRepository.getImagesCardData(2)

    }

    override fun buildModels() {
        var i: Long = 0

        //TODO hardcoded the length as of now, can be changed according to the data sets from remote
        for(j in 0 until 10) {

            carouselCard.forEach {
                CarouselCardAdapter_(it, context)
                    .id(i++)
                    .addTo(this)
            }

            singleDescCrad.forEach {
                SingleDescriptionCardAdapter_(it)
                    .id(i++)
                    .addTo(this)
            }


            imageDescCard.forEach {
                ImageDescriptionCardAdapter_(it)
                    .id(i++)
                    .addTo( this)
            }

            imagesCard.forEach {
                ImagesCardAdapter_(it)
                    .id(i++)
                    .addTo(this)
            }
        }


    }

}