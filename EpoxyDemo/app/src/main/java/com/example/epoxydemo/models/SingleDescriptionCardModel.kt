package com.example.epoxydemo.models

/**
 * Created by Yashaswi NP on 28/10/19.
 */
data class SingleDescriptionCardModel (
    val title:String=""
)