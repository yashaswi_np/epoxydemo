package com.example.epoxydemo.views.adapters

import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.epoxydemo.R
import com.example.epoxydemo.models.ImageDescriptionCardModel
import com.example.epoxydemo.views.KotlinHolder

@EpoxyModelClass(layout = R.layout.image_description_card_layout)
abstract class ImageDescriptionCardAdapter(@EpoxyAttribute var modelData: ImageDescriptionCardModel) :
    EpoxyModelWithHolder<ImageDescriptionCardAdapter.ImageDescriptioncardViewHolder>() {

    override fun bind(holder: ImageDescriptioncardViewHolder) {
        super.bind(holder)
        holder.imageDesc.text = modelData.title
        holder.imageLogo.setImageResource(modelData.image)

    }

    inner class ImageDescriptioncardViewHolder : KotlinHolder() {
        val imageLogo by bind<ImageView>(R.id.image_desc_card_logoIV)
        val imageDesc by bind<TextView>(R.id.image_desc_card_descTV)
    }
}