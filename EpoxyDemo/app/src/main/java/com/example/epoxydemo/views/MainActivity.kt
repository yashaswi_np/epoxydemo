package com.example.epoxydemo.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.epoxydemo.R
import com.example.epoxydemo.viewmodels.CardsDataController
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by Yashaswi NP on 25/10/19.
 */
class MainActivity : AppCompatActivity() {

    private val cardsDataController : CardsDataController by lazy { CardsDataController(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecycler()
    }


    //initializes the epoxy recyclerview
    private fun initRecycler(){
        val linearLayoutManager = LinearLayoutManager(this)
        main_RV.apply{
            layoutManager = linearLayoutManager
            setHasFixedSize(true)
            adapter = cardsDataController.adapter
            addItemDecoration(DividerItemDecoration(this@MainActivity, linearLayoutManager.orientation))
        }
        //This statement builds model and add it to the recycler view
        cardsDataController.requestModelBuild()
    }
}
