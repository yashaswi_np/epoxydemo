package com.example.epoxydemo.repositories


import com.example.epoxydemo.R
import com.example.epoxydemo.models.CarouselCardModel
import com.example.epoxydemo.models.ImageDescriptionCardModel
import com.example.epoxydemo.models.ImagesCardModel
import com.example.epoxydemo.models.SingleDescriptionCardModel
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Yashaswi NP on 28/10/19.
 */
object CardsDataRepository{

    /**
     *     TODO if the data comes from remote,
     *     api call happens here and based on the type sets the data to the different model and been used by controller
     */


    //region Random Data Generators
    private val random = Random()

    //TODO fetch from string resource, context isn't available so hardcoded
    private val lorem : String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"

    private val titles = arrayListOf<String>("Kuliza 1", "Kuliza 2", "Kuliza 3", "Kuliza 4")
    private val descriptions = arrayListOf<String>(lorem, lorem, lorem, lorem)

    //generates the random title
    private fun randomTitle() : String {
        val title = random.nextInt(4)
        return titles[title]
    }

    //generates the random description
    private fun randomDescription() : String {
        val desc = random.nextInt(4)
        return descriptions[desc]
    }

    //generate the random images
    private fun randomPicture() : Int{
        val grid = random.nextInt(7)

        return when(grid) {
            0 -> R.drawable.images
            else -> R.drawable.images
        }
    }

    //sets the mock data for single description card
    fun getSingleDescData(count:Int) : List<SingleDescriptionCardModel>{
        var singleCardDesc = mutableListOf<SingleDescriptionCardModel>()
        repeat(count){
            val descs = randomDescription()
            singleCardDesc.add(SingleDescriptionCardModel(descs))
        }
        return singleCardDesc
    }


    //sets the mock data for ImageDescription card
    fun getImageDescData(count:Int) : List<ImageDescriptionCardModel>{
        var imageDesc = mutableListOf<ImageDescriptionCardModel>()
        repeat(count){
            val title = randomDescription()
            val logo  = randomPicture()
            imageDesc.add(ImageDescriptionCardModel(title,logo))
        }
        return imageDesc
    }



    //sets the mock data for images card
    fun getImagesCardData(count:Int) : List<ImagesCardModel>{
        var images = mutableListOf<ImagesCardModel>()
        repeat(count){
            val title1 = randomTitle()
            val title2 = randomTitle()
            val logo1  = randomPicture()
            val logo2  = randomPicture()
            images.add(ImagesCardModel(logo1,logo2,title1,title2))
        }
        return images
    }

    //sets the mock data for carousel card
    fun getCarouselCardData(count: Int): List<CarouselCardModel> {
        var carouselItem = ArrayList<ImageDescriptionCardModel>()
        var carousel = mutableListOf<CarouselCardModel>()
        repeat(count){
            val title = randomTitle()
            val image = randomPicture()
            for(i in 0 until 10) {
                 carouselItem.add(ImageDescriptionCardModel(randomTitle(), randomPicture()))
            }
            carousel.add(CarouselCardModel(carouselItem))
        }
        return carousel
    }
}