package com.example.epoxydemo.views.adapters

import android.content.Context
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.epoxydemo.R
import com.example.epoxydemo.models.CarouselCardModel
import com.example.epoxydemo.views.KotlinHolder

@EpoxyModelClass(layout = R.layout.carousel_card_layout)
abstract class CarouselCardAdapter(@EpoxyAttribute var modelData: CarouselCardModel,private var context: Context) :
    EpoxyModelWithHolder<CarouselCardAdapter.CarouselCardViewHolder>() {


    override fun bind(holder: CarouselCardViewHolder) {
        super.bind(holder)
        val linearLayoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        holder.recyclerView.apply{
            layoutManager = linearLayoutManager
            setHasFixedSize(true)
            adapter = CarouselItemAdapter(modelData,context)
            addItemDecoration(DividerItemDecoration(context, linearLayoutManager.orientation))
        }
    }


    inner class CarouselCardViewHolder : KotlinHolder() {
        val recyclerView by bind<RecyclerView>(R.id.carousel_card_RV)
    }

}