package com.example.epoxydemo.models

/**
 * Created by Yashaswi NP on 28/10/19.
 */
data class ImageDescriptionCardModel(
val title:String = "",
val image:Int = -1

)
