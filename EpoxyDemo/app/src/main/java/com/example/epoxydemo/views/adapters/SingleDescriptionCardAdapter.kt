package com.example.epoxydemo.views.adapters

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.epoxydemo.R
import com.example.epoxydemo.models.SingleDescriptionCardModel
import com.example.epoxydemo.views.KotlinHolder

@EpoxyModelClass(layout = R.layout.single_description_card_layout)
abstract class  SingleDescriptionCardAdapter(@EpoxyAttribute var modelData : SingleDescriptionCardModel): EpoxyModelWithHolder<SingleDescriptionCardAdapter.SingleDescriptionCardViewHolder>(){

    override fun bind(holder: SingleDescriptionCardViewHolder) {
        holder.titleView.text = modelData.title
    }

    inner class SingleDescriptionCardViewHolder : KotlinHolder(){

        val titleView by bind<TextView>(R.id.single_desc_card_titleTV)
    }
}