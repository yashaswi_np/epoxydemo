package com.example.epoxydemo.views.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.example.epoxydemo.R
import kotlinx.android.synthetic.main.carousel_card_item_layout.view.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import com.example.epoxydemo.models.CarouselCardModel

/**
 * Created by Yashaswi NP on 28/10/19.
 */

class CarouselItemAdapter(private val dataModel: CarouselCardModel, private val context: Context) : RecyclerView.Adapter<CarouselCardViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselCardViewHolder {
        return CarouselCardViewHolder(LayoutInflater.from(context).inflate(R.layout.carousel_card_item_layout, parent, false))
    }

    override fun onBindViewHolder(holder: CarouselCardViewHolder, position: Int) {
        holder.title.text = dataModel.items[position].title
        holder.logo.setImageResource(dataModel.items[position].image)
    }

    override fun getItemCount(): Int {
        return dataModel.items.size
    }

}

class CarouselCardViewHolder (view: View) : RecyclerView.ViewHolder(view){

    @Nullable
    val title: TextView = view.item_carousel_title
    @Nullable
    val logo: ImageView = view.item_carousel_logo
}