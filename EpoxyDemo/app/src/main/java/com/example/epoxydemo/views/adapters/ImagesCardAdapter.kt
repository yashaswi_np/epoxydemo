package com.example.epoxydemo.views.adapters

import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.epoxydemo.R
import com.example.epoxydemo.models.ImagesCardModel
import com.example.epoxydemo.views.KotlinHolder

@EpoxyModelClass(layout = R.layout.images_card_layout)
abstract class ImagesCardAdapter(@EpoxyAttribute var modelData : ImagesCardModel) :
    EpoxyModelWithHolder<ImagesCardAdapter.ImagesCardViewHolder>() {

    override fun bind(holder: ImagesCardViewHolder) {
        super.bind(holder)
        holder.firstImageTitle.text = modelData.firstImageTitle
        holder.secondImageTtle.text = modelData.firstImageTitle
        holder.firstImageLogo.setImageResource(modelData.firstImageLogo)
        holder.secondImageLogo.setImageResource(modelData.secondImageLogo)
    }

    inner class ImagesCardViewHolder : KotlinHolder() {

        val firstImageLogo by bind<ImageView>(R.id.images_card_firstImage_logoIV)
        val secondImageLogo by bind<ImageView>(R.id.images_card_secondImage_logoIV)
        val firstImageTitle by bind<TextView>(R.id.images_card_firstImage_titleTV)
        val secondImageTtle by bind<TextView>(R.id.images_card_secondImage_titleTV)
    }

}